# How to use config scripts for weighted.cc



## Direct configurables:
- weights: name of the sweights from the fit





## Iterable objects (all Optional):
- plots: plot sweighted distribution of variable, lower and upper borders are taken from workspace

        plots
        {
             Lb_PT {                         ;variable name
                title "Lambdab PT sweighted" ;histogram title
                xtitle "PT [MeV]"            ; histogram x-axis title
                bins 200                     ;number of bins
             }
        }

- plots_binning: like plots, with customizable binning


        plots_binning
        {
             Lb_PT {
                title "Lambdab PT sweighted" 
                xtitle "PT [MeV]"
                bins 200
                xlow 0                        ;lower border
                xhigh 3e+5                    ;uppper border
             }

        }

- plots2D: 2D sweighted plot

        plots2D
        {
          LcDalitz {
            varx m2pLcKLc
            vary m2KLcpiLc
            binsx 30
            binsy 30
          }
        }


- plots2D_binning: like plots2D with customizable binning
        plots2D_binning
        {
                  LcDalitz {
            varx m2pLcKLc
            vary m2KLcpiLc
            binsx 30
            binsy 30
            xlow 2000000
            xhigh 4.8e+6
            ylow 350000
            yhigh 1.9e+6
          }

        }

- plots3D: 3D sweighted plots: on the x-axis the variables defined in plots_binning are plotted, the y and z variables and their binning are defined in plots3D (->all 3D plots have the same variable on the y- and z-axis)

        plots3D
        {

          binsy 100
          ylow 0
          yhigh 20000
          yvar Lc_PT
          binsz 100
          zlow 0
          zhigh 10000
          zvar Lc_FDCHI2_OWNPV

        }




