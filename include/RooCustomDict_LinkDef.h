#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class RooRelBreitWigner;
#pragma link C++ class RooIpatia2;
#pragma link C++ class RooJohnsonSU;

#pragma link C++ defined_in "RooRelBreitWigner.h";
#pragma link C++ defined_in "RooIpatia2.h";
#pragma link C++ defined_in "RooJohnsonSU.h";

#endif /* __CINT__ */

