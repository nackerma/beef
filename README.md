# Contributing and Reusing
This is an open analysis tool. Please have a look at our [guidelines](CONTRIBUTING.md) for contributions. If you want to reuse this code in your own project please note the [reciprocity](RECIPROCITY.md) statement.

# Quick start

Build and install `beef` (on LxpLus)
```
lb-run ROOT bash
export CC=gcc
export CXX=g++
mkdir build
cd build
cmake ..
make
cd ..
```

Generate some toy data:
```
root -b -q src/generateToy.C
```

Create workspace called toyws.root
```
build/bin/createWorkspace -i toy.root -c config/config.info -o toyws.root -t toy
```
Have a look at the config file to see how to define more complex workspaces.

Perform the fit defined in `config/fitconfig.info`
```
build/bin/beef -c config/fitconfig.info -i toyws.root -o toyfit.root
```

Look at the fitresults (png, C and pdf files are created automatically).
The dataset including sweights, model etc. are saved in the workspace of the output file.
Inspect it with
```
> root -l toyfit.root
root [1] _file0->ls()
root [2] RooWorkspace* w = static_cast<RooWorkspace*>(_file0->Get("w"))
root [3] w->Print()
```
The fitted parameter values can then be seen using 
```
root [4] auto fr = static_cast<RooFitResult*>(w->obj("<RooFitResult name>"))
root [5] fr->Print()
```

# Writing config files
To configure `beef`, two options files are needed. <br>
One to set up the [RooWorkspace](https://root.cern.ch/doc/master/classRooWorkspace.html "RooWorkspace") which contains all relevant data, defines the observable, and can be used to setup custom pdfs. <br>
The other config file is parsed when running the fit and producing a plot. <br>
The usage of the options files is documented in [doc/README_createWorkspace.md](doc/README_createWorkspace.md) and [doc/README_beef.md](doc/README_beef.md).

# Plotting from a previous fit
There is a dedicated tool, [beefPlot](src/beefPlot.cpp "beefPlot"), that allows to re-create a plot without having to run the fit again.
This is meant for plot-cosmetics and debugging, and is therefore not build by default. To be able to use it, you either have to call the target manually like `make beefPlot`,
or have pass a flag to cmake `-DBEEF_PLOT`.
The tool picks up the output from beef and reads in the fitted pdf and data for plotting. The config file can be the same as the one for fitting. Command line parameters are the same as beef's.

# Snakemake
In many analysis the `snake` workflow is used to combine many dependent analysis steps. The workflow contains so called rules whose structure has the general form 
```
rule rulename:
   input:
      "inputfile1",
      "inputfile2"
   output:
      "outputfile"
   shell:
      'some shell commands'
```

Running the workflow the first rule will run. This should be the final result of the workflow including all dependencies in the inputs. The workflow searches for the rules which output is the input of the starting rule and then searches for their input and so on until every input exists in the workflow (An input is not nessecary for a rule). The workflow now runs every associated rule in the right order to create the inputs of the starting rule. <br>
The whole example above is combined to a workflow in the [Snakefile](Snakefile) <br>
To set up the environment on lxplus6 run
```
source Env_lxplus6.sh
```
Here you can see a [general description](https://gitlab.cern.ch/nskidmor/Analysis_examples#software-environment) of setting up the environment. <br>
With a working environment you can run the workflow with 
```
snakemake
```