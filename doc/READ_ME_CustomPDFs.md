# How to implement new PSF's in beef 

The following document is a small step-by-step guide on how to implement new custom PDF's in beef.

## 1. Location of the C++ files

Copy your header file (e.g. RooMyNewPDF.h) into the folder ../beef/include. 
Copy your cpp file (e.g. RooMyNewPDF.cpp) into the folder ../beef/src.

## 2. Changes to the Workspace

In order for your RooWorkspace to be setup properly you also need to make changes to ../beef/src/createWorkspace.cc.
Simply include your header file in the beginning of the document: 

//ROOFIT
'#include "RooRealVar.h"'
'#include "RooDataSet.h"'
'#include "RooWorkspace.h"'
'#include "RooKeysPdf.h"'
'#include "RooGaussian.h"'
'#include "RooFFTConvPdf.h"'
'#include "RooMyNewPDF.h'   <== Your new PDF

## 3. Changes to the Link_Def file

You will also need to make changes to ../beef/include/RooCustomDict_LinkDef.h. In this file simply add the two lines 

' #pragma link C++ class RooMyNewPDF'

and 

'#pragma link C++ defined_in "RooMyNewPDF.h"'


In total the file should look like this:


'#ifdef __CINT__'

'#pragma link off all globals;'
'#pragma link off all classes;'
'#pragma link off all functions;'
'#pragma link C++ nestedclasses;'

'#pragma link C++ class RooRelBreitWigner;'
'#pragma link C++ class RooIpatia2;'
'#pragma link C++ class RooJohnsonSU;'

'#pragma link C++ defined_in "RooRelBreitWigner.h";'
'#pragma link C++ defined_in "RooIpatia2.h";'
'#pragma link C++ defined_in "RooJohnsonSU.h";'

'#endif /* __CINT__ */'


## 4. Changes to CMakeLists

The last thing you need to do is make changes to ../beef/src/CMakeLists.txt.
Here you have to add the following section:

'#custom dictionaries and libraries'
'set(CMAKE_INSTALL_LIBDIR ${CMAKE_BINARY_DIR}/lib)'
'ROOT_GENERATE_DICTIONARY(RooCustomDict ../include/RooMyNewPDF.h MODULE RooCustomLib LINKDEF ../include/RooCustomDict_LinkDef.h)'
'add_library(RooCustomLib SHARED RooMyNewPDF.cpp RooCustomDict.cxx)'
'target_link_libraries(RooCustomLib PUBLIC ${MY_ROOT_LIBS}) '


If you want to add several new PDF's simply add them to this section, for example:


'#custom dictionaries and libraries'
'set(CMAKE_INSTALL_LIBDIR ${CMAKE_BINARY_DIR}/lib)'
'ROOT_GENERATE_DICTIONARY(RooCustomDict ../include/RooMyNewPDF1.h ../include/RooMyNewPDF2.h ../include/RooMyNewPDF3.h MODULE RooCustomLib LINKDEF ../include/RooCustomDict_LinkDef.h)'
'add_library(RooCustomLib SHARED RooMyNewPDF1.cpp RooMyNewPDF2.cpp RooMyNewPDF3.cpp RooCustomDict.cxx)'
'target_link_libraries(RooCustomLib PUBLIC ${MY_ROOT_LIBS}) '


## 5. Write your PDF into the config file 

Congratulations, you can now use your PDF in the config file for your fit!


