//from std
#include <string>
#include <iostream>
#include <vector>
//from BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/replace.hpp>
//from ROOT
#include "TEfficiency.h"
#include "TString.h"
#include "TFile.h"
#include "TStopwatch.h"
#include "TIterator.h"
//from ROOFIT
#include "RooAbsPdf.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooSimWSTool.h"
#include "RooWorkspace.h"
//from ROOSTATS
#include "RooStats/SPlot.h"
//local
#include "../include/plotter.h"
#include "../IOjuggler/debug_helpers.h"
#include "../IOjuggler/IOjuggler.h"

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner(){return 0;}

//replace some substring in the "name" variable, to be correctly handled by Tex
//digits are formatted into words (0 --> Zero, 1 --> One, ...)
std::string formatTex_towords(std::string name, std::vector<std::pair<std::string,std::string>> texrepl){

  //replace the digits with the words
  for(const auto& r : texrepl)
    boost::algorithm::replace_all(name, r.first,r.second); //replace all digits by their names                                                                                       
  
  //remove the non alphabetical chars
  name.erase(std::remove_if(name.begin(), name.end(),[](const auto& c){return !std::isalpha(c);}), name.end());
  
  return name;
}

//replace some substring in the "name" variable, to be correctly handled by Tex
//digits are formatted into words (Zero --> 0, One --> 1, ...)
std::string formatTex_todigits(std::string name, std::vector<std::pair<std::string,std::string>> texrepl){
  
  //replace the words with the digits
  for(const auto& r : texrepl)
    boost::algorithm::replace_all(name, r.second,r.first);
  
  //remove the non alpha-numerical chars
  name.erase(std::remove_if(name.begin(), name.end(),[](const auto& c){return !(std::isalpha(c) || std::isdigit(c));}), name.end());
  
  return name;
}

int main(int argc, char** argv){

  //////////////////////////////////////////////////////////
  ///  parse command-line options and get first objects  ///
  //////////////////////////////////////////////////////////
  TStopwatch clock;
  clock.Start();
  auto options = parse_options(argc, argv, "c:d:hi:o:r:v:w:p:","nonoptions: cutstring",0);
  MessageService msgsvc("beef",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  //open file, check for absolute path and a path with prepended workdir
  const auto wd     = options.get<std::string>("workdir");
  const auto ofn    = options.get<std::string>("outfilename");
  const auto prefix = options.get<std::string>("prefix","");
  const auto wsn    = options.get<std::string>("wsname");
  auto w = get_obj<RooWorkspace>(get_file(options.get<std::string>("infilename"),wd),wsn);
  //parse nonoptions. they are used for a (single) cutstring and/or for constraining/fixing paramters from earlier fitresults
  std::string selcmdl = "";
  std::vector<TString> cfv;
  for(const auto& nonopt : *options.get_child_optional("extraargs")) {
    TString extraoptStr = nonopt.second.data();
    if(extraoptStr.Contains(":"))
      cfv.push_back(extraoptStr);
    else selcmdl = static_cast<std::string>(extraoptStr);
  }

  ///////////////////////////////
  ///  runtime configuration  ///
  ///////////////////////////////
  pt::ptree configtree = get_ptree(options.get<std::string>("config"));
  replace_stuff_in_ptree(configtree,"{prefix}",prefix,"");
  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);
  SetRootVerbosity(configtree.get_optional<int>("suppress_RootInfo"));

  /////////////////////
  ///  build model  ///
  /////////////////////
  if(!configtree.get_child_optional("model")) throw std::runtime_error("child \"model\" not found in config file");
  msgsvc.infomsg("Model: ");
  for(const auto &v : configtree.get_child("model")) {
    std::string component = v.second.data();
    msgsvc.infomsg("      " + component);
    w->factory(component.c_str());
  }
  const auto modelname = configtree.get_optional<std::string>("pdf");
  if(!modelname) throw std::runtime_error("\"pdf\" not found in config file");
  std::string model_name_for_fit = *modelname;

  const auto sim_node = configtree.get_child_optional("simultaneous");
  if(sim_node){//build simultaneous model
    model_name_for_fit = configtree.get("sim_pdf","sim_model");
    if(const auto sp = configtree.get_optional<std::string>("simultaneous.SplitParam"))
      w->factory(TString::Format("SIMCLONE::%s(%s,%s)",model_name_for_fit.data(),modelname->data(),sp->data()).Data());
    else throw std::runtime_error("\"simultaneous.SplitParam\" not found");
    if(const auto esp = configtree.get_optional<std::string>("simultaneous.SplitParamEdit"))
      w->factory(esp->data());
  }
  auto PDF = get_wsobj<RooAbsPdf>(w,model_name_for_fit);

  ////////////////////////////
  ///  prepare dataset(s)  ///
  ////////////////////////////
  std::string ds_name_for_fit = configtree.get("dsName","Dred");
  //cuts can have 2 sources: config file and command line
  auto selection = configtree.get<std::string>("selection","");

  bool parse_sim_cut_from_nonopt = false;
  if(sim_node)
    if(!((*sim_node).front().first.find("SplitParam") == std::string::npos))
      parse_sim_cut_from_nonopt = true;

  //only add to selection if no explicit cuts are defined in simultaneous node
  if(!parse_sim_cut_from_nonopt && !selcmdl.empty()) selection += selcmdl;

  //get dataset with potential cuts and weights
  auto weight = configtree.get<std::string>("weight","");
  // get dataset
  auto dsFull = get_wsobj<RooDataSet>(w,options.get<std::string>("dsname"));
  msgsvc.infomsg(TString::Format("%.0f candidates in dataset", dsFull->sumEntries()));

  if(!selection.empty())
    msgsvc.infomsg("Selection: " + selection);

  if(!weight.empty()){
    if(!configtree.get_optional<bool>("fitoptions.SumW2Error")){
      msgsvc.warningmsg("You are working with a weighted dataset, but did not make use of the SumW2Error option.");
      msgsvc.warningmsg("The uncertainties returned by the fit will only be correct if your weights are properly normalised!");
    }
    if(auto owfs = configtree.get_optional<std::string>("weight_function")){
      auto weight_var = get_wsobj<RooRealVar>(w,weight);
      RooFormulaVar wFunc("wfunc","event weight",(*owfs).data(),*weight_var);
      auto addedwf = static_cast<RooRealVar*>(dsFull->addColumn(wFunc));
      weight = addedwf->GetName();
    }
  }
  RooDataSet ds(ds_name_for_fit.data(),ds_name_for_fit.data(),dsFull,*dsFull->get(),selection.data(),weight.data());
  msgsvc.infomsg(TString::Format("%.0f candidates selected", ds.sumEntries()));
  w->import(ds);

  if(sim_node){
    msgsvc.infomsg("Splitting dataset for a simultaneous fit");
    ds_name_for_fit = configtree.get("dsCombName","ds_comb");
    auto cat = get_wsobj<RooCategory>(w,static_cast<RooSimultaneous*>(PDF)->indexCat().GetName());//in your face, RooFit!
    RooDataSet ds_comb(ds_name_for_fit.data(),ds_name_for_fit.data(),RooArgSet(*ds.get(),*cat));

    auto add_cat_and_ds = [&w,&ds_comb,&cat,&ds,&msgsvc] (auto&& cat_name, auto&& cut) -> void {
      RooDataSet ds_partial(("ds_"+cat_name).data(),("ds_"+cat_name).data(),&ds,*ds.get(),cut.data());
      cat->setLabel(cat_name.data());
      ds_partial.addColumn(*cat);
      msgsvc.infomsg(TString::Format("Selected %.0f candidates for category \'%s\' with cut %s", ds_partial.sumEntries(),cat_name.data(),cut.data()));
      ds_comb.append(ds_partial);
      w->import(ds_partial);
    };

    if(parse_sim_cut_from_nonopt){//with one cut (from the command line), there are only two possibilities: pass or fail
      add_cat_and_ds(static_cast<std::string>("pass"),selcmdl);
      add_cat_and_ds(static_cast<std::string>("fail"),"!("+selcmdl+")");
    }
    else
      for(const auto& catn : *sim_node)
        if(catn.first.find("SplitParam") == std::string::npos)
          add_cat_and_ds(catn.first,catn.second.get_value<std::string>());

    w->import(ds_comb);
  }

  /////////////////////
  ///  prepare fit  ///
  /////////////////////
  /// fitoptions
  //declare lists that we need for the fit
  RooArgSet parameters_for_minos;
  RooLinkedList llist;
  std::vector<RooCmdArg*> args;
  //fill 'em all
  args.push_back( new RooCmdArg( RooFit::Save() ) );//we want to save the fitresult, so this is not optional
  if(configtree.get_optional<bool>("fitoptions.Extended"))                   args.push_back( new RooCmdArg( RooFit::Extended()) );
  if(configtree.get_optional<bool>("fitoptions.SumW2Error"))                 args.push_back( new RooCmdArg( RooFit::SumW2Error(true)) );
  if(configtree.get_optional<bool>("fitoptions.InitialHesse"))               args.push_back( new RooCmdArg( RooFit::InitialHesse()) );
  if(configtree.get_optional<bool>("fitoptions.Verbose"))                    args.push_back( new RooCmdArg( RooFit::Verbose()) );
  if(configtree.get_optional<bool>("fitoptions.WarningsOff"))                args.push_back( new RooCmdArg( RooFit::Warnings(false)) );
  if(configtree.get_optional<bool>("fitoptions.Timer"))                      args.push_back( new RooCmdArg( RooFit::Timer()) );
  if(auto opt = configtree.get_optional<int> ("fitoptions.Strategy"))        args.push_back( new RooCmdArg( RooFit::Strategy(*opt) ) );
  if(auto opt = configtree.get_optional<int> ("fitoptions.PrintLevel"))      args.push_back( new RooCmdArg( RooFit::PrintLevel(*opt) ) );
  if(auto opt = configtree.get_optional<int> ("fitoptions.PrintEvalErrors")) args.push_back( new RooCmdArg( RooFit::PrintEvalErrors(*opt) ) );
  if(auto opt = configtree.get_optional<std::string>("fitoptions.MinimizerType"))
    args.push_back( new RooCmdArg( RooFit::Minimizer((*opt).c_str(),configtree.get("fitoptions.MinimizerAlg","migrad").c_str()) ) );
  if(auto opt = configtree.get_optional<int>("fitoptions.NumCPU"))
    args.push_back( new RooCmdArg( RooFit::NumCPU(*opt,configtree.get("fitoptions.StratCPU",0)) ) );
  if(auto pois = configtree.get_child_optional("fitoptions.Minos")){
    for(const auto& poi : *pois){
      parameters_for_minos.add(*get_wsobj<RooAbsArg>(w,poi.first));
      msgsvc.infomsg("Adding " + poi.first + " to MINOS arguments");
    }
    args.push_back( new RooCmdArg( RooFit::Minos(parameters_for_minos)) );
  }
  else if(configtree.get_optional<bool>("fitoptions.Minos"))
    args.push_back( new RooCmdArg( RooFit::Minos()) );

  ///////////////
  /// constraints
  //prepare constraints
  RooArgSet constrainFuncs;
  auto constrain_parameter = [&w,&msgsvc,&constrainFuncs,&args] (auto&& pn, const double& val, const double& err = 0.0) -> void {
    auto pdfPar = get_wsobj<RooRealVar>(w,pn);
    pdfPar->setVal(val);//set it as starting parameter
    if(err > 0.0){//allow to set starting parameter without constraining if err < 0
      msgsvc.infomsg(TString::Format("Constraining %s to %.5g +- %.5g",pn.data(),val,err));
      w->factory(TString::Format("Gaussian::constr_%s(%s,mean_%s[%.9e],width_%s[%.9e])",pn.data(),pn.data(),pn.data(),val,pn.data(),err).Data());
      constrainFuncs.add(*get_wsobj<RooAbsArg>(w,"constr_"+pn));
    }
    else if(err == 0.0){
      msgsvc.infomsg(TString::Format("Setting %s to %.5g",pn.data(),val));
      pdfPar->setConstant();
    }
    args.push_back( new RooCmdArg(RooFit::ExternalConstraints(constrainFuncs)));
  };

  // constrain fit parameters from values in config file (optional)
  if(auto cps = configtree.get_child_optional("constrainParams"))
    for(auto v : *cps)
      constrain_parameter(v.first,v.second.get<double>("value"),v.second.get("error",0.0));

  // constrain fit parameters from values of previous fits (optional)
  for(auto& ftwc : cfv){
    const auto setParsNode = configtree.get_child_optional("setParams");
    if(!setParsNode){
      msgsvc.errormsg("Missing \'setParams\' node in fitconfig. Constraints can't be done...");
      break;
    }

    TObjArray *extraoptArr = ftwc.Tokenize(":");
    if(extraoptArr->GetEntries() != 2 || extraoptArr->GetEntries() != 3)
      std::runtime_error("Invalid syntax of nonoptions. Try <childName:fitresultsFile[:fitresultsWorkspace]>");

    const auto paramsNodeName = static_cast<std::string>("setParams."+static_cast<TObjString*>(extraoptArr->At(0))->GetString());
    const auto parChild = configtree.get_child_optional(paramsNodeName);
    if(!parChild)
      msgsvc.errormsg("Missing child \'" + paramsNodeName + "\' in setParams child in fitconfig");

    const auto ftogetname = static_cast<std::string>(static_cast<TObjString*>(extraoptArr->At(1))->GetString());
    msgsvc.infomsg("Constraining or fixing parameters from node \'" + paramsNodeName + "\' and file \'" + ftogetname + "\'");
    const auto wstogetname = extraoptArr->GetEntries() == 3 ?
                               static_cast<std::string>(static_cast<TObjString*>(extraoptArr->At(2))->GetString()) : wsn;
    auto wsToGet = get_obj<RooWorkspace>(get_file(ftogetname,wd),wstogetname);
    if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) wsToGet->Print();

    // loop over all parameters to set
    for(const auto& it : *parChild){
      if(!it.second.data().empty()){//check if this is a valid key value pair
        auto varToSet_name = it.first;
        msgsvc.debugmsg("At " + varToSet_name);
        bool fix_param = boost::algorithm::starts_with(varToSet_name,"fix");
        try {
          auto varToGet = get_wsobj<RooRealVar>(wsToGet,it.second.data());
          if(fix_param)
            constrain_parameter(varToSet_name.erase(0,3),varToGet->getVal());
          else
            constrain_parameter(varToSet_name,varToGet->getVal(),varToGet->getError());
        }
        catch(std::exception& e){
          msgsvc.debugmsg("caught exception: " + static_cast<std::string>(e.what()));
          msgsvc.warningmsg("Skipping " + varToSet_name);
        }
      }
    }
  }

  for (auto arg : args){
    //the next line forbids usage of RooCmdArgs as r-value,
    //also taking the adress of a reference and casting to TObject* can't be handled by RooFit, thus the pointers
    arg->setProcessRecArgs(true,false);
    llist.Add(arg);
  }

  /////////////////////
  ///  execute fit  ///
  /////////////////////
  auto DATA = get_wsobj<RooDataSet>(w,ds_name_for_fit);

  RooFitResult* fr = nullptr;
  if(configtree.get("fitoptions.FitAsHist",0)){
    msgsvc.infomsg("Converting dataset to histogram and performing a binned fit");
    //auto hist = ds.binnedClone(); <-- doesn't work...
    RooArgSet observables_for_hist;
    if(!configtree.get_child_optional("observables"))
      throw std::runtime_error("\"observables\" not found in config file");
    for(const auto& obs : configtree.get_child("observables")){
      auto myobs = get_wsobj<RooRealVar>(w,obs.first);
      myobs->setBins(configtree.get("plots."+obs.first+"plot.bins",100));
      observables_for_hist.add(*myobs);
    }
    RooDataHist hist("hist","hist",observables_for_hist,*DATA);
    fr = PDF->fitTo(hist,llist);
  }
  else
    fr = PDF->fitTo(*DATA,llist);

  for (auto arg : args)
    delete arg;
  args.clear();
  llist.Clear();
  parameters_for_minos.Clear();

  /////////////////////////
  ///  process results  ///
  /////////////////////////
  int status = 0;
  const auto edm = fr->edm();
  const auto fs  = fr->status();
  const auto cq  = fr->covQual();
  if(edm > 0.001)
    msgsvc.warningmsg("fit has large estimated distance to minimum (edm): "+std::to_string(edm));
  if(fs != 0){
    status += fs;
    msgsvc.warningmsg("fit has bad fit status: "+std::to_string(fs));
  }
  if(cq != 3){
    msgsvc.warningmsg("fit has bad covariance matrix: "+std::to_string(cq));
    if(configtree.get("no_bad_cov",1))
      status += cq*100;
  }
  if(status != 0)
    msgsvc.warningmsg("Bad fit status: " + std::to_string(status));

  //print fitresults to stdout
  if(configtree.get("printFitResult",0))
    PrintFitresult(*fr,msgsvc);
  if(configtree.get("printCorrelation",0))
    PrintCorrelationMatrixT(fr->correlationMatrix(),msgsvc);

  //////////////////////////
  /// output to disk : plots
  // create control plot
  const auto plotnodeopt = configtree.get_child_optional("plots");
  if(plotnodeopt){
    for(const auto& plot : *plotnodeopt)
      if(plot.second.get_optional<std::string>("var"))//there could be REPLACE/APPEND nodes
        plotter(w,*DATA,PDF,ofn,wd,prefix,plot.second,msgsvc);
  }
  else if(!plotnodeopt && sim_node){
    auto titer = get_wsobj<RooCategory>(w,static_cast<RooSimultaneous*>(PDF)->indexCat().GetName())->typeIterator();
    while(const auto vcat = titer->Next())
      if(const auto& simpnode = configtree.get_child_optional("plots_"+static_cast<std::string>(vcat->GetName())))
        for(const auto& sim_plot : *simpnode)
          if(sim_plot.second.get_optional<std::string>("var"))
            plotter(w,*DATA,PDF,ofn,wd,prefix,sim_plot.second,msgsvc);
  }

  //////////////////////////////
  /// output to disk : root file
  const auto outfilename = wd+"/"+ofn;
  //create a new workspace to avoid seg-faults arising from cached pdfs like RooFFTConvPdf when trying to access the old workspace in the new file
  auto outfile = TFile::Open(outfilename.data(),"RECREATE");
  RooWorkspace ws("w",true);
  //we don't wan't Splot to screw up our nice fitresult, so we save it already now
  ws.import(*fr);
  ws.import(*PDF);
  //helper variables
  if(const auto auxs = configtree.get_child_optional("auxiliaries"))
    for(const auto& aux : *auxs)
      ws.factory(aux.second.get_value<std::string>().data());


  //////////////////////////////
  /// output to disk : info file
  //make this global for the efficiencies. could be solved by dumping everything into the workspace as well, but meh...
  pt::ptree ptfr;
  const auto infoout = boost::algorithm::replace_all_copy(outfilename,".root",".info");
  if(const auto infonode = configtree.get_child_optional("saveFitResultsAsInfo")){//also true if it's only a key-value pair
    msgsvc.infomsg("Writing info output to " + infoout);
    RooArgSet params(fr->floatParsFinal(),fr->constPars());
    params.add(ws.allFunctions());
    auto pars_for_correlations = fr->floatParsFinal();
    const bool save_corr = static_cast<bool>(configtree.get("saveCorrelationsAsInfo",0));
    auto write_param_to_info = [&ptfr,&fr,&pars_for_correlations,&save_corr] (const auto& param){
      if(param == nullptr || !param->InheritsFrom("RooAbsReal"))
        throw std::runtime_error("param is nullptr or doesn't inherit from RooAbsReal");
      const std::string varname = param->GetName();
      ptfr.add(varname + ".Value",static_cast<RooAbsReal*>(param)->getVal());
      if(param->InheritsFrom("RooRealVar")){
        auto p = static_cast<RooRealVar*>(param);
        ptfr.add(varname + ".Error",p->getError());
        if(p->hasAsymError()){
          ptfr.add(varname + ".ErrorLo",p->getErrorLo());
          ptfr.add(varname + ".ErrorHi",p->getErrorHi());
        }
        if(save_corr && pars_for_correlations.contains(*p)){
          pars_for_correlations.remove(*p);//don't run twice and diagonal
          auto cpit = pars_for_correlations.createIterator();
          while (auto vfr = cpit->Next())
            ptfr.add("Corr_" + varname + "_" + vfr->GetName(),fr->correlation(*p,*static_cast<RooRealVar*>(vfr)));
        }
      }
      else
        ptfr.add(varname + ".Error",static_cast<RooAbsReal*>(param)->getPropagatedError(*fr));
    };
    if(configtree.get("saveFitResultsAsInfo",0)){//false if it's a "real" ptree child
      auto fpit = params.createIterator();
      while (auto var_fitres = fpit->Next())
        write_param_to_info(var_fitres);
    }
    else if(infonode){
      for(const auto& infoit : *infonode){
        try {
          write_param_to_info(params.find(infoit.first.data()));
        }
        catch(std::exception& e){
          msgsvc.warningmsg("caught exception " + static_cast<std::string>(e.what()));
          msgsvc.warningmsg("can't find " + infoit.first + " in fitresult to add it to tex output. skipping it...");
        }
      }
    }
  }

  /////////////////////////////
  /// output to disk : tex files
  
  if(const auto TeXnode = configtree.get_child_optional("saveFitResultsAsTeX")){//also true if it's only a key-value pair

    //stream for the fit results defined as Tex constants
    auto texout  = boost::algorithm::replace_all_copy(outfilename,".root",".tex");
    std::ofstream out_tex;
    msgsvc.infomsg("Writing the fit results as TeX constants to " + texout);
    out_tex.open(texout);

    //stream for the fit results defined as Tex constants
    auto texout_table  = boost::algorithm::replace_all_copy(outfilename,".root","_table.tex");
    std::ofstream out_tex_table;
    msgsvc.infomsg("Writing the fit results as TeX table to " + texout_table);
    out_tex_table.open(texout_table);

    //let's open the table
    out_tex_table << "\\begin{table}[h]" << std::endl;
    out_tex_table << "\\begin{center}" << std::endl;

    //two columns: fit parameters, and fitted values
    out_tex_table << "\\begin{tabular}{c | c}";
    
    out_tex_table << "\\toprule" << std::endl;

    //labels of the columns
    out_tex_table << " Fit parameter & Fitted value \\\\" << std::endl;
    
    out_tex_table << "\\midrule" << std::endl;
    
    ///////////
    
    RooArgSet params(fr->floatParsFinal(),fr->constPars());
    params.add(ws.allFunctions());

    // to enable the writing of the correlations
    auto pars_for_correlations = fr->floatParsFinal();
    const bool save_corr = static_cast<bool>(configtree.get("saveCorrelationsAsTex",1));

    //correlation matrix
    std::map<std::string, std::map<std::string, double>> correlation_matrix;
      
    //define vector for characters which get replaced
    std::vector<std::pair<std::string,std::string>> texrepl =
    {{"0","Zero"},{"1","One"},{"2","Two"},{"3","Three"},{"4","Four"},{"5","Five"},{"6","Six"},{"7","Seven"},{"8","Eight"},{"9","Nine"}};

    auto write_param_to_tex = [&out_tex,&out_tex_table,&texrepl,&fr,&pars_for_correlations,&save_corr,&configtree,&correlation_matrix] (const auto& param){

      if(param == nullptr || !param->InheritsFrom("RooAbsReal"))
        throw std::runtime_error("param is nullptr or doesn't inherit from RooAbsReal");

      //get the Tex prefix, and attach it to the variable name
      //this is used to define the fitted values as Tex constants
      std::string tex_prefix_formatted = configtree.get<std::string>("TeX_prefix","");
      std::string name = static_cast<std::string>(param->GetName());

      //format the name
      //digits to words, and format the underscore
      name = formatTex_todigits(name, texrepl);
      
      //format the tex prefix
      //digits to words
      tex_prefix_formatted = formatTex_towords(tex_prefix_formatted, texrepl);
      
      std::string name_withprefix = tex_prefix_formatted + static_cast<std::string>(param->GetName());

      //format the variable names used to define Tex constants
      //digits to words
      name_withprefix = formatTex_towords(name_withprefix, texrepl);
            
      //value of the parameter
      double value_param = static_cast<RooAbsReal*>(param)->getVal();

      if(param->InheritsFrom("RooRealVar")){
        auto p = static_cast<RooRealVar*>(param);
	
	//round the error to the two most significative digits
	double error = p->getError();

	double factor = 1.;
	
	if(error != 0.){ // otherwise it will return 'nan' due to the log10() of zero                                                                                                    
	  factor = pow(10.0, 2 - std::ceil(log10(fabs(error))));
	  error = std::round(error * factor) / factor;
	}
	
	//round the value of the parameter accordingly to the uncertainty
	value_param = std::round(value_param * factor) / factor;
	
	//write the fit results as Tex constants, with the errors
	out_tex << "\\def \\" << name_withprefix << "{" << value_param << "}" << std::endl;
        out_tex << "\\def \\" << name_withprefix << "Error{" << error << "}" << std::endl;
	
	if(p->hasAsymError()){
          out_tex << "\\def \\" << name_withprefix << "ErrorLo{" << p->getErrorLo() << "}" << std::endl;
          out_tex << "\\def \\" << name_withprefix << "ErrorHi{" << p->getErrorHi() << "}" << std::endl;
        }

	//write the fit results and errors as Tex table
	//skip the parameters which were kept fixed, resulting in 0. error
	if(error != 0.){
	  if(!p->hasAsymError())
	    out_tex_table << name << " & " << value_param << " $\\pm$ " << error << " \\\\" << std::endl;
	  else
	    out_tex_table << name << " & " << value_param
			  << " $^{+" << p->getErrorHi() << "}_{-" << p->getErrorLo() << "}$ \\\\" << std::endl;
	}
	
	//write the correlations
	if(save_corr && pars_for_correlations.contains(*p)){
          pars_for_correlations.remove(*p);//don't run twice and diagonal
          auto cpit = pars_for_correlations.createIterator();
	    
          while (auto vfr = cpit->Next()){

	    //add the first variable to the correlation matrix
	    std::map<std::string, double> temp;

	    //remove from the variable name the TeX prefix
            std::string name_nicer = name_withprefix;
            boost::replace_all(name_nicer, tex_prefix_formatted, "");
	    
	    correlation_matrix.insert(std::pair<std::string, std::map<std::string, double>>(name_nicer, temp));
	    
	    std::string name_2 = vfr->GetName();

	    //format the variable name
	    //digits to words
	    name_2 = formatTex_towords(name_2, texrepl);

	    //round the up to the two most significant digits
	    double correlation = fr->correlation(*p,*static_cast<RooRealVar*>(vfr));

	    if(correlation != 0.){ // otherwise it will return 'nan' due to the log10() of zero
	      double factor = pow(10.0, 2 - std::ceil(log10(fabs(correlation))));
	      correlation = std::round(correlation * factor) / factor; 
	    }
	    
	    out_tex << "\\def \\" << (name_withprefix + "Corr" + name_2) << "{" << correlation << "}" << std::endl;

	    //remove from the variable name the TeX prefix
	    std::string name_2_nicer = name_2;
            boost::replace_all(name_2_nicer, tex_prefix_formatted, "");

	    //format the variable name
            //words to digits
            name_2_nicer = formatTex_todigits(name_2_nicer, texrepl);
	    
	    //add the second variable to the matrix, with the correlation wrt the first variable
	    correlation_matrix[name_nicer].insert(std::pair<std::string, double>(name_2_nicer, correlation));

	    //just to be sure, fill also the symmetric way
	    //otherwise the last parameter won't be printed
	    correlation_matrix.insert(std::pair<std::string, std::map<std::string, double>>(name_2_nicer, temp));
	    correlation_matrix[name_2_nicer].insert(std::pair<std::string, double>(name_nicer, correlation));
	  }
	}  //write the correlations

      }
      else{
	out_tex << "\\def \\" << name_withprefix << "{" << value_param << "}" << std::endl;
	out_tex << "\\def \\" << name_withprefix << "Error{" << static_cast<RooAbsReal*>(param)->getPropagatedError(*fr) << "}" << std::endl;
      }
    };
      
    if(configtree.get("saveFitResultsAsTeX",0)){//false if it's a "real" ptree child
      auto fpit = params.createIterator();
      while (auto var_fitres = fpit->Next())
        write_param_to_tex(var_fitres);
    }
    else if(TeXnode){
      for(const auto& TeXit : *TeXnode){
        try {
          write_param_to_tex(params.find(TeXit.first.data()));
        }
        catch(std::exception& e){
          msgsvc.warningmsg("caught exception " + static_cast<std::string>(e.what()));
          msgsvc.warningmsg("can't find " + TeXit.first + " in fitresult to add it to tex output. skipping it...");
        }
      }
    }

    //let's close the table
    out_tex_table << "\\bottomrule" << std::endl;
    out_tex_table << "\\end{tabular}" << std::endl;
    out_tex_table << "\\end{center}" << std::endl;
    out_tex_table << "\\caption{A nice table created automatically by beef.}" << std::endl;
    out_tex_table << "\\label{tab:fit_results}" << std::endl;
    out_tex_table << "\\end{table}" << std::endl;
    
    //close the tex streams
    out_tex.close();
    out_tex_table.close();
    
    //now write out the correlation matrix
    std::ofstream out_corrmatrix_tex;

    //format the output file name
    auto texout_correlations_matrix  = boost::algorithm::replace_all_copy(outfilename, ".root", "_corrmatrix.tex");
    msgsvc.infomsg("Writing the correlation matrix as TeX table to " + texout_correlations_matrix);
    out_corrmatrix_tex.open(texout_correlations_matrix);
    
    double correlation;

    //let's open the table
    out_corrmatrix_tex << "\\begin{table}[h]" << std::endl;

    //you want to resize the tables: I put the command there but I comment it
    //uncomment it directly in the Tex if you really need that
    out_corrmatrix_tex << "%\\resizebox{1.5\textwidth}{!}{" << std::endl;

    out_corrmatrix_tex << "\\begin{center}" << std::endl;
    out_corrmatrix_tex << "\\begin{tabular}{c |";

    //I have to add the specify number of columns = number of parameters
    for(std::map<std::string, std::map<std::string, double>>::const_iterator it_param = correlation_matrix.begin();
        it_param != correlation_matrix.end(); ++it_param)
      out_corrmatrix_tex << " c";

    out_corrmatrix_tex << "}" << std::endl;

    out_corrmatrix_tex << "\\toprule" << std::endl;

    //now it's time for the name of the parameters,
    //to be placed on top of the table
    for(std::map<std::string, std::map<std::string, double>>::const_iterator it_param = correlation_matrix.begin();
	it_param != correlation_matrix.end(); ++it_param)
      out_corrmatrix_tex << " & " << formatTex_todigits(it_param->first, texrepl);
    
    out_corrmatrix_tex << " \\\\" << std::endl;
    out_corrmatrix_tex << "\\midrule" << std::endl;

    //loop over the parameters stored in the correlation matrix
    for(std::map<std::string, std::map<std::string, double>>::const_iterator it_first = correlation_matrix.begin();
	it_first != correlation_matrix.end(); ++it_first){

      //get the name of the current variable
      std::string varname_1 = it_first->first;
      
      out_corrmatrix_tex << formatTex_todigits(varname_1, texrepl) << "\t & ";
      
      //loop over all the parameters!
      for(std::map<std::string, std::map<std::string, double>>::const_iterator it_second = correlation_matrix.begin();
	  it_second != correlation_matrix.end(); ++it_second){

	std::string varname_2 = it_second->first;
	
	//correlation of the variable with itself? Put it to 1 and go to the next variable.
	//Otherwise retrieve it from the map, and continue
	if(varname_1 == varname_2){
	  out_corrmatrix_tex << "1. \\\\" << std::endl;
	  break;
	}
	else{
	  correlation = correlation_matrix[varname_1][varname_2];
	  out_corrmatrix_tex << correlation << " & ";
	}
      }  //loop over the second variables
    }  //loop over the first variables

    //let's close the table
    out_corrmatrix_tex << "\\bottomrule" << std::endl;
    out_corrmatrix_tex << "\\end{tabular}" << std::endl;
    out_corrmatrix_tex << "\\end{center}" << std::endl;

    //this is related to the resizebox command (see above), commented by default
    out_corrmatrix_tex << "%}" << std::endl;

    out_corrmatrix_tex << "\\caption{A nice huge table created automatically by the beef.}" << std::endl;
    out_corrmatrix_tex << "\\label{tab:correlations}" << std::endl;
    out_corrmatrix_tex << "\\end{table}" << std::endl;
    
    out_corrmatrix_tex.close();
  }

  ////////////////
  /// efficiencies
  if(configtree.get_child_optional("efficiency")){
    //calculate the efficiency
    const auto Nsig_pass_var = get_wsobj<RooRealVar>(w,configtree.get("efficiency.NSigPassName","NSig_pass"));
    const auto Nsig_fail_var = get_wsobj<RooRealVar>(w,configtree.get("efficiency.NSigFailName","NSig_fail"));
    auto p = Nsig_pass_var->getVal(); // number of measured accepted events
    auto f = Nsig_fail_var->getVal(); // number of measured rejected events
    //p and f should be allowed to fluctuate to negative values
    //push them back to their domain, TEfficiency will return bad results otherwise
    if(p < 0.0){
      f += p;
      p = 0.0;
    }
    if(f < 0.0){
      p += f;
      f = 0.0;
    }
    const auto dp = Nsig_pass_var->getError();
    const auto df = Nsig_fail_var->getError();
    //measured efficiency
    const auto eff = p/(p+f);

    // lambda to get binomial signal errors
    auto eo = static_cast<TEfficiency::EStatOption>(configtree.get<int>("efficiency.StatisticsOption",0));
    const auto cl = configtree.get<double>("efficiency.ConfidenceLevel",0.6827);
    const auto alpha = configtree.get<double>("efficiency.alpha",0.5);
    const auto beta = configtree.get<double>("efficiency.beta",0.5);
    auto get_efferr = [&eo,&cl,&alpha,&beta,&eff] (const double& et, const double& ep, const bool& upper) {
      if(eo == TEfficiency::kFCP)
        return TEfficiency::ClopperPearson(et,ep,cl,upper)-eff;
      else if(eo == TEfficiency::kFNormal)
        return TEfficiency::Normal(et,ep,cl,upper)-eff;
      else if(eo == TEfficiency::kFWilson)
        return TEfficiency::Wilson(et,ep,cl,upper)-eff;
      else if(eo == TEfficiency::kFAC)
        return TEfficiency::AgrestiCoull(et,ep,cl,upper)-eff;
      else if(eo == TEfficiency::kFFC)
        return TEfficiency::FeldmanCousins(et,ep,cl,upper)-eff;
      else if(eo == TEfficiency::kBJeffrey)
        return TEfficiency::Bayesian(et,ep,cl,0.5,0.5,upper)-eff;
      else if(eo == TEfficiency::kBUniform)
        return TEfficiency::Bayesian(et,ep,cl,1.,1.,upper)-eff;
      else if(eo == TEfficiency::kBBayesian)
        return TEfficiency::Bayesian(et,ep,cl,alpha,beta,upper)-eff;
      else{
        throw std::range_error("Only valid from 0-7 or in TEfficiency namespace: kFCP, kFNormal, "
                               "kFWilson, kFAC, kFFC, kBJeffrey, kBUniform, kBBayesian");
        return -66.6;
      }
    };
    //get confidence interval: calculate uncertainty propagation.
    // account for nuisance parameters with scaling the propagated uncertainty to binomial interval with full statistics
    const auto s1st = std::pow(f*dp,2)/std::pow(p+f,4);
    const auto s2nd = std::pow(p*df,2)/std::pow(p+f,4);
    const auto s3rd = -2*p*f*eff*(1-eff)*(std::pow(dp,2)+std::pow(df,2)+2*eff*(1-eff)*(p+f))/std::pow(p+f,4);
    const auto vareff_prop = s1st+s2nd+s3rd;
    //scaling
    const auto wald_var = eff*(1-eff)/(p+f);
    const auto wald_scl = wald_var/vareff_prop;
    const auto befl = -get_efferr(p+f,p,false);
    const auto befh =  get_efferr(p+f,p,true);
    //use scaling to Wald if it makes sense
    auto scl = 0.f;
    if(0.05 < wald_scl && wald_scl < 20. && 0.667 < befl/befh && befl/befh < 1.5)
      scl = wald_scl;
    else
      scl = eff > 0.5 ? std::pow(befl,2)/vareff_prop : std::pow(befh,2)/vareff_prop;
    //get the uncertainties
    const auto deff_lo = -get_efferr(scl*(p+f),scl*p,false);
    const auto deff_hi =  get_efferr(scl*(p+f),scl*p,true);
    //print efficiency result
    msgsvc.infomsg(std::string(128, '*'));
    msgsvc.infomsg(TString::Format("* Scaling factor : %.7g,  effective Ntot: %.0f,  effective Npass: %.0f",scl,scl*(p+f),scl*p));
    msgsvc.infomsg(TString::Format("* Signal efficiency = ( %7.5g +%.3f -%.3f ) %%",100*eff,100*deff_hi,100*deff_lo));
    msgsvc.infomsg(std::string(128, '*'));

    //write ptree with results
    ptfr.add("sig_eff.Value",eff);
    ptfr.add("sig_eff.ErrorLo",deff_lo);
    ptfr.add("sig_eff.ErrorHi",deff_hi);

    //if you can't decide which limit you like...
    if(configtree.get("efficiency.get_all_limits",0)){
      std::vector<std::string> statnames = {"ClopperPearson","Normal","Wilson","AgrestiCoull","FeldmanCousins","BayesJeffrey","BayesUniform","BayesBeta"};
      int statoptenum = 0;
      for(const auto& sn : statnames){
        eo = static_cast<TEfficiency::EStatOption>(statoptenum++);
        const auto del = -get_efferr(scl*(p+f),scl*p,false);
        const auto deh =  get_efferr(scl*(p+f),scl*p,true);
        msgsvc.infomsg(TString::Format("%-19.19s = ( %7.5g +%.3f -%.3f ) %%",sn.data(),100*eff,100*deh,100*del));
        ptfr.add(sn + ".ErrorLo",del);
        ptfr.add(sn + ".ErrorHi",deh);
      }
      ptfr.add("Wald.Error",std::sqrt(wald_var));
      ptfr.add("Prop.Error",std::sqrt(vareff_prop));
    }

    RooRealVar RCVeff("eff_sig","eff_sig",eff);//i have no clue why, but importing a RooConstVar doesnt work here (it worked in other scripts, and yes, i included the header)
    RooRealVar RCVeff_lo("deff_sig_lo","deff_sig_lo",deff_lo);
    RooRealVar RCVeff_hi("deff_sig_hi","deff_sig_hi",deff_hi);
    ws.import(RCVeff);
    ws.import(RCVeff_lo);
    ws.import(RCVeff_hi);
    const auto nbkgpname = configtree.get_optional<std::string>("efficiency.NBkgPassName");
    const auto nbkgfname = configtree.get_optional<std::string>("efficiency.NBkgFailName");
    if(nbkgpname && nbkgfname){
      const auto bp = get_wsobj<RooRealVar>(w,*nbkgpname)->getVal();
      const auto bf = get_wsobj<RooRealVar>(w,*nbkgfname)->getVal();
      RooRealVar RCVbeff("eff_bkg","eff_bkg",bp/(bp+bf));
      ws.import(RCVbeff);
    }
  }
  if(!ptfr.empty()) pt::info_parser::write_info(infoout,ptfr);

  //////////////////////
  /// calculate sweights
  if(const auto extchild = configtree.get_child_optional("extended")){
    std::vector<std::string> exn;
    for(const auto& extpar : *extchild)
      exn.push_back(extpar.first);
    //Now we can get the sWeights. we need to set all shape parameters constant and put the yield parameters in a RooArgList
    RooArgList extended_parameters;
    //wtf roofit? why does static_cast<RooArgSet>(fitresult->floatParsFinal()).createIterator() give complete nonsense?
    const auto afps = static_cast<RooArgSet>(fr->floatParsFinal());
    auto fpit = afps.createIterator();
    while(auto fp = static_cast<RooRealVar*>(fpit->Next())){
      if(std::find_if(exn.begin(),exn.end(),[&fp](const auto& nm){return nm == fp->GetName();}) != exn.end()){
        extended_parameters.add(*fp);
        msgsvc.infomsg("Will calculate sweights for " + static_cast<std::string>(fp->GetName()));
      }
      else fp->setConstant();//i think RooStats would handle this internally, but rather keep it like this and add some more lines below, since the fit validation has been done with this setting.
    }
    RooStats::SPlot("dummy","An SPlot",*DATA,PDF,extended_parameters);
    //unfreeze the parameters again (see statement 3 lines above)
    fpit->Reset();
    while(auto fp = static_cast<RooRealVar*>(fpit->Next()))
      fp->setConstant(false);
  }

  //sweights are saved by importing data
  ws.import(*DATA);
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) ws.Print();
  ws.Write();
  outfile->Write();
  //cleanup
  outfile->Delete("*");
  outfile->Close("R");
  delete w;
  delete fr;
  delete outfile;

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();

  return 0;
}
