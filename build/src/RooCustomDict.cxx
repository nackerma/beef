// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME RooCustomDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/work/ackermann/beef/src/../include/RooRelBreitWigner.h"
#include "/work/ackermann/beef/src/../include/RooIpatia2.h"
#include "/work/ackermann/beef/src/../include/RooJohnsonSU.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_RooRelBreitWigner(void *p = 0);
   static void *newArray_RooRelBreitWigner(Long_t size, void *p);
   static void delete_RooRelBreitWigner(void *p);
   static void deleteArray_RooRelBreitWigner(void *p);
   static void destruct_RooRelBreitWigner(void *p);
   static void streamer_RooRelBreitWigner(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooRelBreitWigner*)
   {
      ::RooRelBreitWigner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooRelBreitWigner >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooRelBreitWigner", ::RooRelBreitWigner::Class_Version(), "RooRelBreitWigner.h", 9,
                  typeid(::RooRelBreitWigner), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooRelBreitWigner::Dictionary, isa_proxy, 16,
                  sizeof(::RooRelBreitWigner) );
      instance.SetNew(&new_RooRelBreitWigner);
      instance.SetNewArray(&newArray_RooRelBreitWigner);
      instance.SetDelete(&delete_RooRelBreitWigner);
      instance.SetDeleteArray(&deleteArray_RooRelBreitWigner);
      instance.SetDestructor(&destruct_RooRelBreitWigner);
      instance.SetStreamerFunc(&streamer_RooRelBreitWigner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooRelBreitWigner*)
   {
      return GenerateInitInstanceLocal((::RooRelBreitWigner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooRelBreitWigner*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooIpatia2(void *p = 0);
   static void *newArray_RooIpatia2(Long_t size, void *p);
   static void delete_RooIpatia2(void *p);
   static void deleteArray_RooIpatia2(void *p);
   static void destruct_RooIpatia2(void *p);
   static void streamer_RooIpatia2(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooIpatia2*)
   {
      ::RooIpatia2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooIpatia2 >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooIpatia2", ::RooIpatia2::Class_Version(), "RooIpatia2.h", 23,
                  typeid(::RooIpatia2), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooIpatia2::Dictionary, isa_proxy, 16,
                  sizeof(::RooIpatia2) );
      instance.SetNew(&new_RooIpatia2);
      instance.SetNewArray(&newArray_RooIpatia2);
      instance.SetDelete(&delete_RooIpatia2);
      instance.SetDeleteArray(&deleteArray_RooIpatia2);
      instance.SetDestructor(&destruct_RooIpatia2);
      instance.SetStreamerFunc(&streamer_RooIpatia2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooIpatia2*)
   {
      return GenerateInitInstanceLocal((::RooIpatia2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooIpatia2*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooJohnsonSU(void *p = 0);
   static void *newArray_RooJohnsonSU(Long_t size, void *p);
   static void delete_RooJohnsonSU(void *p);
   static void deleteArray_RooJohnsonSU(void *p);
   static void destruct_RooJohnsonSU(void *p);
   static void streamer_RooJohnsonSU(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooJohnsonSU*)
   {
      ::RooJohnsonSU *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooJohnsonSU >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooJohnsonSU", ::RooJohnsonSU::Class_Version(), "RooJohnsonSU.h", 27,
                  typeid(::RooJohnsonSU), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooJohnsonSU::Dictionary, isa_proxy, 16,
                  sizeof(::RooJohnsonSU) );
      instance.SetNew(&new_RooJohnsonSU);
      instance.SetNewArray(&newArray_RooJohnsonSU);
      instance.SetDelete(&delete_RooJohnsonSU);
      instance.SetDeleteArray(&deleteArray_RooJohnsonSU);
      instance.SetDestructor(&destruct_RooJohnsonSU);
      instance.SetStreamerFunc(&streamer_RooJohnsonSU);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooJohnsonSU*)
   {
      return GenerateInitInstanceLocal((::RooJohnsonSU*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooJohnsonSU*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooRelBreitWigner::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooRelBreitWigner::Class_Name()
{
   return "RooRelBreitWigner";
}

//______________________________________________________________________________
const char *RooRelBreitWigner::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWigner*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooRelBreitWigner::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWigner*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooRelBreitWigner::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWigner*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooRelBreitWigner::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWigner*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooIpatia2::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooIpatia2::Class_Name()
{
   return "RooIpatia2";
}

//______________________________________________________________________________
const char *RooIpatia2::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooIpatia2*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooIpatia2::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooIpatia2*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooIpatia2::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooIpatia2*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooIpatia2::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooIpatia2*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooJohnsonSU::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooJohnsonSU::Class_Name()
{
   return "RooJohnsonSU";
}

//______________________________________________________________________________
const char *RooJohnsonSU::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooJohnsonSU*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooJohnsonSU::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooJohnsonSU*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooJohnsonSU::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooJohnsonSU*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooJohnsonSU::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooJohnsonSU*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void RooRelBreitWigner::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooRelBreitWigner.

   RooAbsPdf::Streamer(R__b);
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooRelBreitWigner(void *p) {
      return  p ? new(p) ::RooRelBreitWigner : new ::RooRelBreitWigner;
   }
   static void *newArray_RooRelBreitWigner(Long_t nElements, void *p) {
      return p ? new(p) ::RooRelBreitWigner[nElements] : new ::RooRelBreitWigner[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooRelBreitWigner(void *p) {
      delete ((::RooRelBreitWigner*)p);
   }
   static void deleteArray_RooRelBreitWigner(void *p) {
      delete [] ((::RooRelBreitWigner*)p);
   }
   static void destruct_RooRelBreitWigner(void *p) {
      typedef ::RooRelBreitWigner current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooRelBreitWigner(TBuffer &buf, void *obj) {
      ((::RooRelBreitWigner*)obj)->::RooRelBreitWigner::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooRelBreitWigner

//______________________________________________________________________________
void RooIpatia2::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooIpatia2.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      l.Streamer(R__b);
      zeta.Streamer(R__b);
      fb.Streamer(R__b);
      sigma.Streamer(R__b);
      mu.Streamer(R__b);
      a.Streamer(R__b);
      n.Streamer(R__b);
      a2.Streamer(R__b);
      n2.Streamer(R__b);
      R__b.CheckByteCount(R__s, R__c, RooIpatia2::IsA());
   } else {
      R__c = R__b.WriteVersion(RooIpatia2::IsA(), kTRUE);
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      l.Streamer(R__b);
      zeta.Streamer(R__b);
      fb.Streamer(R__b);
      sigma.Streamer(R__b);
      mu.Streamer(R__b);
      a.Streamer(R__b);
      n.Streamer(R__b);
      a2.Streamer(R__b);
      n2.Streamer(R__b);
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooIpatia2(void *p) {
      return  p ? new(p) ::RooIpatia2 : new ::RooIpatia2;
   }
   static void *newArray_RooIpatia2(Long_t nElements, void *p) {
      return p ? new(p) ::RooIpatia2[nElements] : new ::RooIpatia2[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooIpatia2(void *p) {
      delete ((::RooIpatia2*)p);
   }
   static void deleteArray_RooIpatia2(void *p) {
      delete [] ((::RooIpatia2*)p);
   }
   static void destruct_RooIpatia2(void *p) {
      typedef ::RooIpatia2 current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooIpatia2(TBuffer &buf, void *obj) {
      ((::RooIpatia2*)obj)->::RooIpatia2::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooIpatia2

//______________________________________________________________________________
void RooJohnsonSU::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooJohnsonSU.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      xMed.Streamer(R__b);
      sigx.Streamer(R__b);
      delta.Streamer(R__b);
      gamma.Streamer(R__b);
      R__b.CheckByteCount(R__s, R__c, RooJohnsonSU::IsA());
   } else {
      R__c = R__b.WriteVersion(RooJohnsonSU::IsA(), kTRUE);
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      xMed.Streamer(R__b);
      sigx.Streamer(R__b);
      delta.Streamer(R__b);
      gamma.Streamer(R__b);
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooJohnsonSU(void *p) {
      return  p ? new(p) ::RooJohnsonSU : new ::RooJohnsonSU;
   }
   static void *newArray_RooJohnsonSU(Long_t nElements, void *p) {
      return p ? new(p) ::RooJohnsonSU[nElements] : new ::RooJohnsonSU[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooJohnsonSU(void *p) {
      delete ((::RooJohnsonSU*)p);
   }
   static void deleteArray_RooJohnsonSU(void *p) {
      delete [] ((::RooJohnsonSU*)p);
   }
   static void destruct_RooJohnsonSU(void *p) {
      typedef ::RooJohnsonSU current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooJohnsonSU(TBuffer &buf, void *obj) {
      ((::RooJohnsonSU*)obj)->::RooJohnsonSU::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooJohnsonSU

namespace {
  void TriggerDictionaryInitialization_libRooCustomLib_Impl() {
    static const char* headers[] = {
"/work/ackermann/beef/src/../include/RooRelBreitWigner.h",
"/work/ackermann/beef/src/../include/RooIpatia2.h",
"/work/ackermann/beef/src/../include/RooJohnsonSU.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_84/ROOT/6.06.02/x86_64-slc6-gcc49-opt/include",
"/work/ackermann/beef/src",
"/work/ackermann/beef/src/../include",
"/auto/lhcb/lib/lcg/releases/ROOT/6.06.02-6cc9c/x86_64-slc6-gcc49-opt/include",
"/auto/sigma0/ackermann/beef/build/src/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libRooCustomLib dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate(R"ATTRDUMP(Relativistic Breit Wigner PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$/work/ackermann/beef/src/../include/RooRelBreitWigner.h")))  RooRelBreitWigner;
class __attribute__((annotate("$clingAutoload$/work/ackermann/beef/src/../include/RooIpatia2.h")))  RooIpatia2;
class __attribute__((annotate(R"ATTRDUMP(Your description goes here...)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$/work/ackermann/beef/src/../include/RooJohnsonSU.h")))  RooJohnsonSU;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libRooCustomLib dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "/work/ackermann/beef/src/../include/RooRelBreitWigner.h"
#include "/work/ackermann/beef/src/../include/RooIpatia2.h"
#include "/work/ackermann/beef/src/../include/RooJohnsonSU.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"RooIpatia2", payloadCode, "@",
"RooJohnsonSU", payloadCode, "@",
"RooRelBreitWigner", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libRooCustomLib",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libRooCustomLib_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libRooCustomLib_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libRooCustomLib() {
  TriggerDictionaryInitialization_libRooCustomLib_Impl();
}
